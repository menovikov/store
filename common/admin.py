from django.contrib import admin

from common.models import (
	StoreItem,
	StoreItemCat,
	Order,
	Profile,
	Cart
	)

class ProfileAdmin(admin.ModelAdmin):
	list_display = ['id']


class OrderAdmin(admin.ModelAdmin):
	list_display = ['id', 'created']


class CartAdmin(admin.ModelAdmin):
	list_display = ['id', 'created']


class StoreItemAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'price', 'category', 'image', 'file']
	list_editable = ['name', 'price', 'category']


class StoreItemCatAdmin(admin.ModelAdmin):
	list_display = ['id', 'name']


admin.site.register(StoreItem, StoreItemAdmin)
admin.site.register(StoreItemCat, StoreItemCatAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Cart, CartAdmin)