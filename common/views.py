import json
from datetime import datetime as dt
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.http import (
	JsonResponse, 
	HttpResponse, 
	HttpResponseRedirect,
	Http404,
	)
from .forms import CheckoutForm, ProfileForm
from .models import (
	StoreItem, 
	StoreItemCat, 
	Cart,
	Order,
	Profile,
	)


def redirect_view(request):
	return redirect('store_items_list')

# @login_required
def logout_view(request):
	logout(request)
	return redirect('store_items_list')


def profile_view(request):
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)
	profile_form = ProfileForm(user=request.user)
	if request.POST.get('save_profile'):
		data = request.POST
		first_name = data.get('first_name')
		last_name = data.get('last_name')
		email = data.get('email')
		phone = data.get('phone')
		payment_card = data.get('payment_card')
		password1 = data.get('password1')
		password2 = data.get('password2')
		
		profile = get_object_or_404(Profile, user=request.user)
		if first_name and first_name != profile.first_name:
			profile.first_name = first_name
		if last_name and last_name != profile.last_name:
			profile.last_name = last_name
		if email and profile.email != email:
			profile.email = email
		if phone and phone != profile.phone:
			profile.phone = phone
		if payment_card and payment_card != profile.payment_card:
			profile.payment_card = payment_card
		profile.save()

		if password1 and password2 and password1 == password2:
			user = request.user
			user.set_password(password1)
			user.save()

		return redirect('profile')

	context = {
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
		'profile_form' : profile_form,
	}
	return render(request, 'profile.html', context)


def make_payment_view(request, id=None):
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)
	order = get_object_or_404(Order, id=id)
	if request.POST.get('orderId'):
		id = int(request.POST.get('orderId'))
		order = get_object_or_404(Order, id=id)
		order.is_paid = True
		order.save()
		return redirect('order_list')
	context = {
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
		'order' : order,
	}
	return render(request, 'make_payment.html', context)


def checkout_view(request):
	form = CheckoutForm()
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)
	if request.POST:
		data = request.POST
		first_name = data.get("first_name")
		last_name = data.get("last_name")
		email = data.get("email")
		phone = data.get("phone")
		payment_card = data.get("payment_card")

		if first_name and last_name\
		and email and phone and payment_card:
			cart = get_object_or_404(Cart, 
				user=request.user, is_archived=False)
			if cart.store_item.all().count() < 1:
				return redirect('store_items_list')
			order = Order()
			order.first_name = first_name
			order.last_name = last_name
			order.user = cart.user
			order.email = email
			order.phone = phone
			order.payment_card = payment_card
			order.total = sum([x.price for x in cart_items])
			order.is_checked_out = True
			order.save()
			cart.order = order
			cart.is_archived = True
			cart.save()
			return HttpResponseRedirect(
				reverse('make_payment', kwargs={'id' : order.id})
				)

		return redirect('store_items_list')
	context = {
		'form' : form,
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
	}
	return render(request, 'checkout.html', context)


def get_cart_items(user):
	if user.is_authenticated():
		try:
			cart = Cart.objects.get(user=user, is_archived=False)
		except:
			cart = Cart.objects.create(user=user, is_archived=False)
		cart_items = StoreItem.objects.filter(cart=cart)
		return cart_items
	return StoreItem.objects.none()


def add_cart_items(user, item_id):
	if user.is_authenticated():
		try:
			cart = Cart.objects.get(user=user, is_archived=False)
		except:
			cart = Cart.objects.create(user=user, is_archived=False)
		item = get_object_or_404(StoreItem, id=item_id)
		cart.store_item.add(item)
		cart_items = StoreItem.objects.filter(cart=cart)
		return cart_items
	return StoreItem.objects.none()

def store_items_list_view(request):
	store_items_list = StoreItem.objects.all().order_by('?')
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)

	context = {
		'categories_list' : categories_list,
		'store_items_list' : store_items_list,
		'cart_items_count' : cart_items.count(),
	}
	return render(request, 'store_items_list.html', context)


def order_list_view(request):
	if not request.user.is_authenticated():
		raise Http404
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)
	orders_list = Order.objects.filter(user=request.user)
	context = {
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
		'orders_list' : orders_list,
	}
	return render(request, 'orders_list.html', context)


def store_item_detail_view(request, id=None):
	item = get_object_or_404(StoreItem, id=id)
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)

	context = {
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
		'item' : item,
	}
	return render(request, 'store_items_detail.html', context)


def cart_item_add_view(request):
	if request.POST:
		if not request.user.is_authenticated():
			raise Http404
		id = int(request.POST.get("itemId"))
		cart_items = add_cart_items(request.user, id)
		context = {
			'cart_items_count' : cart_items.count(),
		}
		return JsonResponse(context, safe=False)
	return HttpResponse(status=404)


def store_items_list_cat_view(request, id=None):
	category = get_object_or_404(StoreItemCat, id=id)
	store_items_list = StoreItem.objects.filter(category=category)
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)

	context = {
		'categories_list' : categories_list,
		'store_items_list' : store_items_list,
		'cart_items_count' : cart_items.count(),
	}
	return render(request, 'store_items_list.html', context)


def search_view(request):
	if request.POST.get('searchQuery'):
		query_string = request.POST.get('searchQuery')
		if query_string and query_string != "" \
			and not (len(set(query_string)) <= 1 \
			and " " in set(query_string)):

			q = Q(name__contains=query_string) | \
				Q(description__contains=query_string) | \
				Q(category__name__contains=query_string)
			result_list = StoreItem.objects.filter(q)
			categories_list = StoreItemCat.objects.all()
			cart_items = get_cart_items(request.user)
			context = {
				'categories_list' : categories_list,
				'store_items_list' : result_list,
				'cart_items_count' : cart_items.count(),
			}
			return render(request, 'store_items_list.html', context)
	
	return redirect('store_items_list')


def cart_view(request):
	categories_list = StoreItemCat.objects.all()
	cart_items = get_cart_items(request.user)
	total = sum([x.price for x in cart_items])
	context = {
		'items' : cart_items,
		'total' : total,
		'categories_list' : categories_list,
		'cart_items_count' : cart_items.count(),
	}
	return render(request, 'cart.html', context)