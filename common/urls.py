from django.conf.urls import url

from common.views import (
	store_items_list_view,
	store_items_list_cat_view,
	store_item_detail_view,
	order_list_view,
	cart_item_add_view,
	cart_view,
	checkout_view,
	logout_view,
	search_view,
	make_payment_view,
	profile_view,
	redirect_view,
	)

urlpatterns = [
	url(r'^items/$', store_items_list_view, name="store_items_list"),
	url(r'^orders/$', order_list_view, name="order_list"),
	url(r'^items/category/(?P<id>\d+)/$', store_items_list_cat_view, 
		name="store_items_list_cat"),
	url(r'^items/(?P<id>\d+)/$', store_item_detail_view, 
		name="store_item_detail"),
	url(r'^items/cart/add/$', cart_item_add_view, 
		name="cart_item_add"),
	url(r'^cart/checkout/$', checkout_view, name="checkout"),
	url(r'^cart/$', cart_view, name="cart"),
	url(r'^profile/$', profile_view, name="profile"),
	url(r'^logout/$', logout_view, name="logout"),
	url(r'^search/$', search_view, name="search_view"),
	# url(r'^(?P<id>\d+)/$', project_details, name="project_details"),
	url(r'^payment/(?P<id>\d+)/$', make_payment_view, 
		name="make_payment"),
	url(r'', redirect_view, name="redirect"),
]
