from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .models import (
	Order,
	)

class CheckoutForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ('first_name', 'last_name', 'email', 'phone', 'payment_card')


class ProfileForm(forms.Form):
	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user')
		super(ProfileForm, self).__init__(*args,**kwargs)

		self.fields['first_name'] = forms.CharField(
			label=_(u"First name"), 
			initial=self.user.first_name,
			required=False,
			)

		self.fields['last_name'] = forms.CharField(
			label=_(u"Last name"), 
			initial=self.user.last_name,
			required=False,
			)

		self.fields['phone'] = forms.CharField(
			label=_(u"Phone"), 
			initial=self.user.profile.phone,
			required=False,
			)

		self.fields['email'] = forms.CharField(
			label=_(u"Email"), 
			initial=self.user.profile.email,
			required=False,
			)

		self.fields['payment_card'] = forms.CharField(
			label=_(u"Payment card"), 
			initial=self.user.profile.payment_card,
			required=False,)

		self.fields['password1'] = forms.CharField(
			label=_(u"Password"), 
			widget=forms.PasswordInput(),
			required=False,)

		self.fields['password2'] = forms.CharField(
			label=_(u"Repeat password"), 
			widget=forms.PasswordInput(),
			required=False,)
