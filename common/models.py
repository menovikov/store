from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    phone = models.CharField(
        max_length=25,
        null=True, 
        blank=True,
    )
    payment_card = models.CharField(max_length=128, null=True, blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
    	profile = Profile()
    	profile.user = instance
    	profile.first_name = instance.first_name
    	profile.last_name = instance.last_name
    	profile.email = instance.email
    	profile.save()

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	profile = instance.profile
	profile.first_name = instance.first_name
	profile.last_name = instance.last_name
	profile.email = instance.email
	profile.save()

@receiver(post_save, sender=Profile)
def save_user_profile(sender, instance, **kwargs):
	user = instance.user
	user.first_name = instance.first_name
	user.last_name = instance.last_name
	user.email = instance.email
	user.save()

# Every time django User is deleted,
# registration user is deleted too
@receiver(pre_delete, sender=User)
def delete_registration_user(sender, instance, **kwargs):
    try:
        from store_reg.models import RegistrationProfile
        RegistrationProfile.objects.get(
            user=instance
        ).delete()
    except Exception:
        pass


class StoreItemCat(models.Model):
	name = models.CharField(max_length=50, null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	
	def __str__(self):
		return self.name

def store_item_image_loc(store_item, filename):
	return "store_item_img/%s/%s" %(store_item.name.replace(' ', '_'), filename)

def template_upload_path(store_item, filename):
    return "templates/%s/%s" % (store_item.id, filename)

class StoreItem(models.Model):
	name = models.CharField(max_length=50, null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	price = models.IntegerField(default=0)
	category = models.ForeignKey(StoreItemCat, null=True, blank=True)
	image = models.ImageField(
        upload_to=store_item_image_loc, 
        null=True, 
        blank=True,
        height_field='height_field', 
        width_field='width_field')
	height_field = models.IntegerField(default=0, null=True, blank=True)
	width_field = models.IntegerField(default=0, null=True, blank=True)
	file = models.FileField(
        upload_to=template_upload_path,
        null=True,
        blank=True,
        )


class Order(models.Model): 
	user = models.ForeignKey(User, null=True, blank=True)
	created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
	is_checked_out = models.BooleanField(default=False)
	is_paid = models.BooleanField(default=False)
	total = models.IntegerField(default=0)
	first_name = models.CharField(max_length=50, null=True, blank=True)
	last_name = models.CharField(max_length=50, null=True, blank=True)
	email = models.EmailField(null=True, blank=True)
	phone = models.CharField(
		max_length=25,
		null=True, 
		blank=True,
	)
	payment_card = models.CharField(max_length=128, null=True, blank=True)

	def get_cart(self):
		return Cart.objects.get(order=self)


class Cart(models.Model):
	store_item = models.ManyToManyField(StoreItem)
	created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
	user = models.ForeignKey(User, null=True, blank=True)
	is_archived = models.BooleanField(default=False)
	order = models.OneToOneField(Order, null=True, blank=True)

	def get_items(self):
		return self.store_item.all()

	def get_items_count(self):
		return self.get_items().count()