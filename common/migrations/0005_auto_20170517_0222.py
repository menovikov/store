# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-05-16 23:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0004_auto_20170517_0119'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cart',
            old_name='moved',
            new_name='is_archived',
        ),
        migrations.RemoveField(
            model_name='order',
            name='store_item',
        ),
        migrations.AddField(
            model_name='order',
            name='is_checked_out',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='cart',
            name='store_item',
            field=models.ManyToManyField(to='common.StoreItem'),
        ),
    ]
