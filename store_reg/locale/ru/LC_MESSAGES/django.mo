��    ;      �  O   �        1   	  %   ;  e   a  f   �  �   .     �     �          .     =     S     `     p     �  5   �     �  	   �     �  -   �       
   !     ,     2  	   @  
   J     U     Z     j     {     �     �     �     �     �     �     �     		     	     4	     D	     \	     k	     x	     �	  B   �	     �	     �	  N   �	  '   9
     a
     �
  (   �
     �
     �
     �
     �
            �  $  E   �  5   0  �   f  d     h  }  4   �  8     3   T  4   �  4   �     �     	  4   '     \  .   c     �     �     �  ]   �  
   /  
   :  
   E     P     c     �     �  *   �     �     �  (   �  +        @     O  9   d  %   �  2   �  (   �  %         F  9   d     �  !   �  
   �  $   �  {        �  J   �  �   �  \   }  K   �  *   &  ^   Q     �     �  '   �  '     7   3     k            7                    (             /               2       	              '             )   $      *   1          0       !                         4   8      +      9   5   #      :      %             ,             ;      6   &            -   3          .         "       
    
    Sincerely,
    %(site_name)s Management
     
Sincerely,
%(site_name)s Management
 
To activate this account, please click the following link within the next
%(expiration_days)s days:
 
To reset your password, please click the following link, or copy and paste it
into your web browser:
 
You (or someone pretending to be you) have asked to register an account at
%(site_name)s.  If this wasn't you, please ignore this email
and your address will be removed from our records.
 Account Activated Account activation failed. Account activation on Activate users Activation email sent Best regards Change password Confirm password reset E-mail Enter your new password below to reset your password: Forgot password? Greetings I agree with I have read and agree to the Terms of Service Log in Logged out Login Login (Email) Main page Management Name Not a user yet? Password changed Password reset Password reset complete Password successfully changed! Phone Proceed Re-send activation emails Recover Register for an account Registered already? Registration is closed Repeat password Resend Activation Email Reset password Set password Sign in Sign up Sorry, but registration is closed at this moment. Come back later. Submit Successfully logged out This email address is already in use. Please supply a different email address. You must agree to the terms to register Your account is now activated. Your password has been reset! Your username, in case you've forgotten: activation key registration registration profile registration profiles terms of service user Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-23 20:40+0300
PO-Revision-Date: 2015-09-09 00:39+0300
Last-Translator: Andrew Grigorev <andrew@ei-grad.ru>
Language-Team: Russian <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 2.0
 
    С уважением,<br>
    Команда %(site_name)s
     
С уважением,
Команда %(site_name)s
 
Чтобы активировать эту учетную запись, перейдите по следующей ссылке
в течении %(expiration_days)s дней:
 
Чтобы сбросить пароль, перейдите по следующей ссылке:
 
Вы (или кто-то выдающий себя за вас) оставил заявку на регистрацию
учетной записи на %(site_name)s. Если это были не вы, то проигнорируйте
это сообщение, и ваш почтовый адрес будет удален из наших записей.
 Учетная запись активирована Активация аккаунта не удалась. Активация учетной записи на Активировать учетные записи Письмо активации отправлено С уважением, Изменить пароль Подтверждение сброса пароля E-mail Введите ваш новый пароль: Забыли пароль? Привет Я согласен с Я прочитал Правила Использования и согласен с ними Войти Выход Войти Логин (email) Главная страница   Имя Еще нет учетной записи? Пароль изменен Сброс пароля Сброс пароля завершен Пароль успешно изменен! Телефон Продолжить Выслать ключи активации заново Восстановить пароль Регистрация учетной записи Уже зарегистрированы? Регистрация закрыта Сбросить пароль Выслать ключи активации заново Сбросить пароль Установить пароль Войти Зарегистрироваться Извините, но регистрация в данный момент закрыта. Попробуйте позже. Отправить Вы успешно вышли из своей учетной записи Этот адрес электронной почты уже используется. Пожалуйста, введите другой адрес. Для регистрации Вы должны согласиться с Правилами Ваша учетная запись теперь активирована. Ваш пароль был сброшен! Ваше имя пользователя, на случай если вы его забыли: ключ активации регистрация карточка регистрации карточки регистрации политикой конфиденциальности пользователь 